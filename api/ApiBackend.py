# contrib 
import json, requests, sys, logging
#from typing import TYPE_CHECKING, Any, Dict, List, cast
from .keys import ThirdPartyIntegrationKeys
from django.conf import settings
logger = logging.getLogger("accounts")

class ApiBackend():
    endpoint = settings.SCHOOL_API_URL + "/ddapi/mailusers"

    def __init__(self):
        self.key_manager  = self.key_start()

    def key_start(self): 
        school_api_domain = "admin." + settings.SCHOOL_DOMAIN + "/ddapi"
        keys = settings.CERT_FOLDER
        key_manager = ThirdPartyIntegrationKeys(our_name="correu", their_name="DD", their_service_domain=school_api_domain, key_store=settings.CERT_FOLDER) 
        their_pub_key = key_manager.their_pubkey_jwk
        return key_manager

    def get_jwt(self):
        return self.key_manager.get_outgoing_request_headers()

    def get_api_users(self):
        result = None
        try:
            response = requests.get(
            self.endpoint,
            headers=self.get_jwt()
            )
            result = self.key_manager.verify_and_decrypt_incoming_json(response.text)
        except requests.exceptions.RequestException as e:
           logger.error(e)
        return result

    
    def post_changes_to_api(self,data):
        enc_data = self.key_manager.sign_and_encrypt_outgoing_json(data)
        try:
            response = requests.post(
            self.endpoint,
            data = enc_data,
            headers=self.get_jwt()
            )
            return response
        except Exception as e:
            logger.error(e)
            return None


