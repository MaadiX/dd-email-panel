from django.contrib import admin
from api.models import AllowedIp
# Register your models here.
class AllowedIpAdmin(admin.ModelAdmin):
    list_display = (
        'ip',
    )
admin.site.register(AllowedIp,AllowedIpAdmin)

