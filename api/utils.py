import json, requests, sys, logging
import os
import secrets
import time
import traceback
from pprint import pprint
from datetime import datetime
from datetime import timedelta
import requests
import socketio
from functools import wraps
from typing import Any, Tuple, Callable, Dict, List
# Project
from django.conf import settings
from accounts.models import Account
from requests import Request
from jose import jwt, jws, jwk, jwe
from jose.constants import ALGORITHMS
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from django.http import JsonResponse
logger = logging.getLogger("accounts")

from .ApiBackend  import ApiBackend

def is_api_user(function):
  @wraps(function)
  def wrap(request,*args, **kwargs):
    payload  = get_jws_payload(request)
    if payload:
        return function(request, *args, **kwargs)
    else:
        response = JsonResponse({'message': 'You are in the wrong place!'})

  return wrap

def get_jws_payload(request) -> Tuple[str, Dict]:
    api_ob = ApiBackend()
    """
    Try to parse the Authorization header into a JWT.
    By getting the key ID from the unverified headers, try to verify the token
    with the associated key.

    For pragmatism it returns a tuple of the (key_id, jwt_claims).
    """
    kid: str = ""
    try:
        t: str = request.headers.get("Authorization", "")
        # Get which KeyId we have to use.
        # We default to 'empty', so a missing 'kid' is not an issue in an on
        # itself.
        # This enables using an empty key in app.api_3p as the default.
        # That default is better managed in AdminFlaskApp and not here.
        kid = jwt.get_unverified_headers(t).get("kid", "")

    except jwt.ExpiredSignatureError:
        logger.debug("JWT Expired" )

    except jwt.JWTClaimsError:
        logger.debug("JWT lain error")
        traceback.format_stack()
    except Exception as e:
        logger.debug("JWT error" )
        print(e)
        traceback.format_stack()
    try:
        # Try to get payload
        return (kid, api_ob.key_manager.verify_incoming_data(t))
    except Exception as e:
        logger.debug("JWT error" )
        traceback.format_stack()
        return JsonResponse({'message': 'You are in the wrong place!'})

def save_encrypted_data(data):
    #TODO: save operation into database with status istead of file
    os.makedirs('/tmp/nc_data', exist_ok=True)
    filename = "/tmp/nc_data/nexctloud_add_%s" % datetime.now().timestamp()
    nc_file =  open(filename, "w")
    nc_file.write(response.text)
    nc_file.close()
    #TODO: save operation into database with status
    return filename

def decrypt_file(f_name):
    with open(f_name, "rb") as key_file:
        f_content = key_file.read()
    decoded = decrypt_jwe(f_content)
    return decoded.decode()


def mail_server_config():
    config_data = {
        "inbound_host"  : settings.IMAP_SERVER,
        "inbound_port"  :  settings.IMAP_PORT,
        "inbound_ssl_mode" : 'ssl',
        "outbound_host" : settings.SMTP_SERVER,
        "outbound_port" :    settings.SMTP_PORT,
        "outbound_ssl_mode": 'ssl',
        }
    return  config_data

def build_users_mail_josn(data):
    nc_json={
        "config" : mail_server_config(), 
        "users" : data
        }
    return json.dumps(nc_json)

