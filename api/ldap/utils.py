# contrib
import ldap3, datetime, time, logging
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from django.utils.translation import ugettext_lazy as _, get_language
# project
from django.conf import settings
# This is to make a ldap connection using provided username for login or binding
# maybe we do not use this with SSO

#TODO: method to close ldap connetion

logger = logging.getLogger("accounts")
def unix_timestamp():
    """ Returns Unix timestamp """
    return int(round(time.time() * 1000))

def ldap_creation_date():
    """ Returns creation date in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y%m%d')
def ldap_creation_dateTime():
    """ Returns creation dateTime in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S') 

def ldap_bool(bool):
    """ Convert a boolean value to its string representation in LDAP """
    if bool:
        return 'TRUE'
    return 'FALSE'

def ldap_val(val):
    """ Convert LDAP val to a valid str representation in Django """
    #Modify the logic to use this function also for text attribute that may be true or false (lowercase)
    if (val == 'TRUE' or val == 'True' or val == 'true'):
        return True
    elif (val == 'FALSE' or val == 'False' or val == 'false'):
        return False
    return val
