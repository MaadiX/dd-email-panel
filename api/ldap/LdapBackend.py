# contrib
import ldap3, datetime, time, logging, sys, json
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE,SUBTREE
from ldap3.utils.hashed import hashed
from django.utils.translation import ugettext_lazy as _, get_language
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
# project
from django.conf import settings
sys.path.append('../api')
from api import utils
from api.ldap import utils as ldaputils
from api.ApiBackend  import ApiBackend
from accounts.models import Account
logger = logging.getLogger("accounts")

"""
Class to Handle ldap operations
"""

class Ldap():

    def __init__(self):
        self.conn = self.ldap_default_conn()

    def ldap_default_conn(self):
        try:
            # Connect ldap
            if settings.AUTH_LDAP_START_TLS:
                auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
            else:
                auto_bind = ldap3.AUTO_BIND_NO_TLS
            ldap = ldap3.Connection(
                ldap3.Server(
                    settings.LDAP_AUTH_URL,
                    allowed_referral_hosts = [("*", True)],
                    get_info               = ldap3.NONE,
                    connect_timeout        = settings.LDAP_AUTH_CONNECT_TIMEOUT,
                ),
                user             = settings.AUTH_LDAP_BIND_DN,
                password         = settings.AUTH_LDAP_BIND_PASSWORD,
                auto_bind        = auto_bind,
                raise_exceptions = True,
                receive_timeout  = settings.LDAP_AUTH_RECEIVE_TIMEOUT,
            )

            # if connection, check permissions and set ldap as a property of
            # the request, available to any view
            if ldap:
                return ldap
                #return ldap
            elif ldap == 'LDAPSocketOpenError':
                # If socket error, ldap is shut down for some reasons
                # warn the user accordingly
                logger.error("Error connecting to ldap")
        except Exception as e:
            logger.error("Ldap Error {}".format(e))

    def unbind(self):
        self.conn.unbind()

    def search(self, dn, ldap_filters, ldap_attributes):
        try:
            self.conn.search(
                dn,
                ldap_filters,
                attributes = ldap_attributes
                )
            return self.conn.entries[0]
        except Exception as e:
            logger.error("Ldap Error {}".format(e))

    def modify_pass(self,dn, value):
         self.conn.modify(dn, { 'userpassword' : [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, value))] })

    def modify(self,dn, data):
        mod_data = {}

        for k, val in data.items(): 
            mod_data[k] = [(MODIFY_REPLACE,val)]

        self.conn.modify(dn, mod_data) 

    #TODO: Change this query filerying by type attribute
    def get_all_emails(self):
        dn="vd=%s,%s" % (settings.SCHOOL_MAIL_DOMAIN, settings.LDAP_TREE_HOSTING)
        try:
            #ldapconn = connect_ldap(settings.AUTH_LDAP_ADMIN_BIND_DN, settings.AUTH_LDAP_BIND_ADMIN_PASSWORD)
            #ldap = ldapconn['connection'] if ldapconn['connection'] else None
            ## Add type = existing_email
            self.conn.search(
                    dn,
                    '(objectClass=VirtualMailAccount)', 
                    attributes=['*'],
                    search_scope=SUBTREE,)
            emails = self.conn.entries
            return emails
            self.conn.unbind()
        except Exception as e:
            logger.error(e)

    def ldap_search_mail(self, mail):
        try:
            self.conn.search(
                settings.LDAP_TREE_HOSTING,
                settings.LDAP_FILTERS_EMAILS,
                attributes=['mail']
            )
            email = self.conn.entries[0].mail.value
        except:
            raise Exception
            logger.debug("No records found")
            email=None
        return email

    def ldap_get_mail_data(self, mail):
        try:
            domain = mail.split('@')[1]
            dn='mail=%s,vd=%s,%s' % (mail,domain,settings.LDAP_TREE_HOSTING)
            self.conn.search(
                dn,
                '(objectClass=VirtualMailAccount)',
                attributes = ['*']
            )
            account = self.conn.entries[0]

        except:
            raise Exception
            logger.debug("No records found")
            account=None
        return account

    def add_mail(self, indata, email, uid):
        opened = False
        domain = email.split("@")[1]
        # TODO: We need to send plain password to dd.
        dn = 'mail=%s,vd=%s,%s' % (email, domain, settings.LDAP_TREE_HOSTING)
        classes= ['top', 'VirtualMailAccount', 'Vacation', 'VirtualForward', 'amavisAccount' ]

        data = {
                'accountActive'           : 'TRUE',
                'lastChange'              : ldaputils.unix_timestamp(),
                'creationDate'            : ldaputils.ldap_creation_date(),
                'amavisspamtaglevel'      : '3.0',
                'amavisspamtag2level'     : '5.5',
                'amavisspamkilllevel'     : '6.0',
                'amavisbypassviruschecks' : 'TRUE',
                'amavisbypassspamchecks'  : 'FALSE',
                'forwardactive'           : 'FALSE',
                'vacationactive'          : 'FALSE',
                'smtpauth'                : 'TRUE',
                'delete'                  : 'FALSE',
                'mailautoreply'           : email+'.autoreply',
                'otherTransport'          : "gnarwl:%s" % domain,
                'uid'                     : uid,
                'mailbox'                 : domain + '/' + uid,
                'vdhome'                  : '/home/vmail/domains',
                'quota'                   : indata["quota"],
                'mail'                    : email,
                'userPassword'            : indata["password"],
                'sn'                      : indata["last_name"],
                'givenname'               : indata["first_name"],
                'cn'                      : indata["first_name"] + ' ' + indata["last_name"],
            }
        try:
            self.conn.add(dn,classes,data)
            return 'created'
        except Exception as e:
            # if account already esist, update it
            # Maybe remove this as we will keep database updated via websocket
            if e.result == 68:
                try:
                    self.conn.modify(dn, {
                        'cn'   : [(MODIFY_REPLACE, data["cn"])],
                        'givenname' : [(MODIFY_REPLACE, data["givenname"])],
                        'sn' : [(MODIFY_REPLACE, data["sn"])],
                        'mail' : [(MODIFY_REPLACE, email)],
                        'quota' : [(MODIFY_REPLACE,data["quota"])],
                        })
                    return 'updated'
                except Exception as e:
                    error = _('Ha habido problemas actualizandola cuenta de correo.')
                    logger.error("Error updating  email account {}".format(e))
            else:
                    error = _('Ha habido problemas creando la cuenta de correo.')
                    logger.error("Error creating email account {}".format(e))
        return 'error'

    def get_allowed_domains(self, filter_query):
        dn=settings.LDAP_ALLOWED_DOMAINS_BASE
        data = {"message" : None, "domains" : None}
        try:
            self.conn.search(
                dn,
                filter_query,
                attributes=['*']
                )
            domains = self.conn.entries
            data["domains"] = domains
        except Exception as e:
            data["message"] = _("No hay resultados")
            msg = "Unsuccessful read  operation: {e}".format(e=e)
            logger.error(msg)
        return data


    def get_whiltelisted_emails(self,filter_query):
        dn=settings.LDAP_WHITELIST_BASE
        data = {"message" : None, "emails" : None}
        try:
            #ldapconn = connect_ldap(settings.AUTH_LDAP_ADMIN_BIND_DN, settings.AUTH_LDAP_BIND_ADMIN_PASSWORD)
            #ldap = ldapconn['connection'] if ldapconn['connection'] else None
            ## Add type = existing_email
            self.conn.search(
                dn,
                filter_query,
                attributes=['mail','sn', 'destinationindicator']
                )
            emails = self.conn.entries
            data["emails"] = emails
        except Exception as e:
            data["message"] = _("No hay resultados")
            msg = "Unsuccessful read  operation: {e}".format(e=e)
            logger.error(msg)
        return data

    def get_whitelisted_email(self, email, filter_query):
        # This query should return a unique entry
        dn= "mail=%s,%s" %(email, settings.LDAP_WHITELIST_BASE)
        data = {}
        emails = None
        try:
            self.conn.search(
                dn,
                filter_query,
                attributes=['*']
                )
            emails = self.conn.entries
            #self.conn.unbind()

        except Exception as e:
            #Entry not found
            logger.error(e)
        return emails

# As there is one unque tree for all allowed emails,
# we use the type attribute to set which school has added
# one specific email account.
# This allwos us to query entries by school
    def add_whitelisted_email(self, mail, display_name):
        dn="mail=%s,%s" % ( mail, settings.LDAP_WHITELIST_BASE)
        # First check if entry already exists
        try:
            # Ix exists, chekc attribute type
            display_name = display_name if display_name else mail
            classes=['top', 'inetOrgPerson']
            data = {
                'mail' : mail,
                'sn'   : display_name,
                'cn'   : display_name,
                'destinationindicator' : settings.IMAP_SERVER,
                #'destinationindicator' : 'correu.test.digitaldemocartic.net',
                }
            added = self.conn.add(dn,classes,data)
        except Exception as e:
            if e.result == 68:
                #Checij if account exists and has already school identifierr
                filter_query = '(&%s(destinationindicator=%s))' % (settings.LDAP_FILTERS_WITELIST,settings.IMAP_SERVER)
                added = self.get_whitelisted_email(mail,filter_query)
                if not added:
                    added = self.conn.modify(dn, {
                        'destinationIndicator'   : [(MODIFY_ADD, settings.IMAP_SERVER)],
                    })
            #if entry does not exists, and could not be createsd, reai ldap error
            else:
                error = _('La cuenta de correo %s no se ha podido añadir' % mail)
                logger.error("Error updating  email account {}".format(e))
                added = None
        # Return for checking add result from view
        return added

    def remove_whitelisted_email(self, mail):
        # A school can only remove a mail added by itself
        # we query exixsting entrie includin the attribute
        # destinationindicator , which may match with
        # settings.IMAP_SERVER which is the fqdn
        dn="mail=%s,%s" % ( mail, settings.LDAP_WHITELIST_BASE)
        filter_query ="(&(objectClass=inetOrgPerson)(destinationindicator=%s))" % settings.IMAP_SERVER
        maild = self.get_whitelisted_email(mail, filter_query)
        print("maild ", maild)
        if maild:
            #We are the only school who added this email
            if len(maild[0].destinationIndicator) > 1:
                try:
                    dn="mail=%s,%s" % ( mail, settings.LDAP_WHITELIST_BASE)
                    self.conn.modify(dn, {
                    'destinationindicator'   : [(MODIFY_DELETE, settings.IMAP_SERVER)],
                    })
                except Exception as e:
                    error = _('La cuenta de correo %s no se ha podido eliminar' % mail)
                    logger.error("Error removing  email account {}".format(e))
            else:
                try:
                    self.conn.delete(dn)
                except Exception as e:
                    error = _('La cuenta de correo %s no se ha podido eliminar' % mail)
                    logger.error("Error removing  email account {}".format(e))
        print("self.conn.result ", self.conn.result)
        return self.conn.result

    def get_user_by_id(self, uid):
        dn="vd=%s,%s" % (settings.SCHOOL_MAIL_DOMAIN, settings.LDAP_TREE_HOSTING)
        try:
            #ldapconn = connect_ldap(settings.AUTH_LDAP_ADMIN_BIND_DN, settings.AUTH_LDAP_BIND_ADMIN_PASSWORD)
            #ldap = ldapconn['connection'] if ldapconn['connection'] else None
            ## Add type = existing_email
            self.conn.search(
                    dn,
                    '(&(objectClass=VirtualMailAccount)(uid=%s))' % uid,
                    attributes=['*'],
                    search_scope=SUBTREE,)
            email = self.conn.entries[0]
            return email
        except Exception as e:
            logger.error("No email account found for users {}".format(uid))

    def add_user_account(self, users):
        # method to add both: dajno user and email
        # users is a json containing user data
        errors = ""
        success_msg = ""
        update_message = ""
        info_msg= ""
        quota = settings.DEFAULT_QUOTA
        # DATA is a list of dict with data for the Account Model objects
        data = {}
        #nc_data is to build the json to send to the api
        nc_data = []

        for user in users:
            user_mail = user["email"]
            # TODO see haow to encript pass in pstgre to send to dd
            pwd        = user.get('password',Account.objects.make_random_password(length=10))
            data["keycloak_id"] = user["keycloak_id"]
            data["mailpassword"] = pwd
            data["first_name"]  =  user["first"]
            data["last_name"]   =  user["last"]
            data["tags"]        = user["groups"]
            data["quota"]       = quota
            data["email"]       = user["email"]
            logger.debug("Processing User %s" % user["username"])
            try:
                # We need to create a Django user 
                us, new_user  = Account.objects.update_or_create(
                    username    = user["username"],
                    defaults = data
                    )
                # If the group does not exixst, create it.
                # Maybe this not correct. ¿Do We just accept limited roles?
                group, new_group = Group.objects.get_or_create(name=user['role'])
                print("GROUP ", group)
                if new_group:
                    ct = ContentType.objects.get_for_model(Account)
                    perm_own_account, perm_create = Permission.objects.get_or_create(codename ='can_edit_own_account',
                                                            name ='Edit own account',
                                                            content_type = ct)
                    group.permissions.add(perm_own_account)
                    # If is manager add additional permissions
                    if group.name == 'manager' or group.name =='teacher':
                        perm_other_accounts,perm_create = Permission.objects.get_or_create(codename ='can_edit_student_account',
                                                            name ='Edit student account',
                                                            content_type = ct)
                        perm_white_list, perm_list_create = Permission.objects.get_or_create(codename ='can_edit_whitelist',
                                                            name ='Edit whitelist',
                                                            content_type = ct)
                        group.permissions.add(perm_other_accounts,perm_white_list)
                # If this is a new user, add it to the croup
                if new_user and group:
                    group.user_set.add(us)
                elif us and group:
                    try:
                        user_group = Account.groups.through.objects.get(account_id=us.id)
                        user_group.group = group
                        user_group.save()
                    except Exception as e:
                        group.user_set.add(us)

                # TODO: we do not nedd a user password, as users will login vial SAML
                # make_random_password is very slow for bulk creation
                #c.password = make_random_password()
                if(new_user):
                    msg = "Saving User To Database: {}".format(us.username)
                else:
                    msg = "Updating Existing User: {}".format(us.username)
                logger.error(msg)
                us.set_password(pwd)
                us.save()
                # Case in which a username (uid) has changed email.
                # uid in ldap matches username in keycloak. Check if the uid already exists.
                # If so, check if the email matches. In this case we just update remaining data.
                # if a uid exists in ldap but email does not mathces anymore we have to delete 
                # existing mail entry and  create a new one . In ldap the dn is mail=mail@account,vd=.....
                # Check if uid exists
                existing_uid = self.get_user_by_id(user["username"])
                if existing_uid and existing_uid.mail != user["email"]:
                    # Remove previous existing ldap entry
                    self.conn.delete(existing_uid.entry_dn)
                #Whit this json we need to notify dd api
                # FOr LDAP we store hashed passowrd
                # Only create ldap email if domain matches school domain
                if(user_mail.split("@")[1].strip() == settings.SCHOOL_MAIL_DOMAIN.strip()):
                    data["password"] = hashed(HASHED_SALTED_SHA, pwd)
                    try:
                        add_mail = self.add_mail(data, user["email"],user["username"] )
                        # If we have created a new email_account in ldap, notify api-sso
                        if add_mail == 'created' or us.first_name != user["first"] or us.last_name != user["last"]:
                            success_msg += user_mail +'\n'
                            logger.debug("User added to LDAP %s" % user["username"])

                            dd_data = {
                            "email"         : user_mail,
                            "user_id"       : user["username"], 
                            "name"          : user["first"]  + ' ' + user["last"],
                            "password"      : pwd,
                            "outbound_user" : user_mail,
                            }
                            # nc_data is a json containing all new created mails accounts
                            # We only notify api if there is some new email
                            nc_data.append(dd_data)
                        elif add_mail == 'updated':
                            update_message += user_mail + '\n'

                    except Exception as err:
                        msg = "Error adding ldap entry: {}".format(err)
                        logger.error(msg)

                else:
                    info_msg += user["email"] + "\n"
            except Exception as e:
                logger.error("Error adding postgre  entry: {}".format(e))
                errors +=   user["username"] + "\n"

        if nc_data:
            # Build a json containing mail s)erver configuration
            send_data = {
                    'config' : utils.mail_server_config(),
                    'users'  : nc_data,
                    }
            print("DAtA to be sent to DD " , send_data) 
            try:
                api_ob = ApiBackend()
                sent_data = api_ob.post_changes_to_api(send_data)

                # post_changes_to_api retuns response code
                # if it fails, save data
                #if sent_data.status_code != 200:
                    #TODO: do something with data: cron job to execute orphane tasks?
                 #   saved_file = utils.save_encrypted_data(encoded_data)
            except Exception as e:
                logger.error('Error sendig data back to API: %s' % e)
        self.unbind()
        messages = {
		'update_message' : update_message,
		'success_msg'	: success_msg,
		'errors'	: errors,
		'info_msg'	: info_msg
	}
        return messages

    def delete_mail_account(self, data):
        #Delete django user and ldap email
        try:
            u = Account.objects.get(keycloak_id = data["keycloak_id"])
            email = u.email
            delete = u.delete()
            domain = email.split("@")[1]
            dn = 'mail=%s,vd=%s,%s' % (email, domain, settings.LDAP_TREE_HOSTING)
            self.conn.delete(dn)
        except Account.DoesNotExist:
            logger.error("User %s does not exist" )	
        except Exception as e:
            logger.error('Error deleting %s' % u.username)
            logger.error(e)
