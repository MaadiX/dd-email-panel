import json, requests, sys, logging
import os
from django.test import TestCase
from django.test.runner import DiscoverRunner
# project
#sys.path.append('../api')
from api import utils
from api.ldap import utils as ldaputils
#from accounts.forms import ImportUsersForm
from django.conf import settings
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from api.ldap.LdapBackend import Ldap

TEST_URL = settings.SCHOOL_API_URL + "/ddapi/mailusers"

class NoDbTestRunner(DiscoverRunner):
    """ A test runner to test without database creation """

    def setup_databases(self, **kwargs):
        """ Override the database creation defined in parent class """
        pass

    def teardown_databases(self, old_config, **kwargs):
        """ Override the database teardown defined in parent class """
        pass

class ApiEnpointTestCase(TestCase):
    #def setUp(self):

    def get_expected_data(self):

        expected_data={

            "config" : {
                "inbound_host"  : settings.IMAP_SERVER,
                "inbound_port"  :  settings.IMAP_PORT,
                "inbound_ssl_mode" : 'ssl',
                "outbound_host" : settings.SMTP_SERVER,
                "outbound_port" :    'ssl',
                "outbound_ssl_mode": settings.MAIL_SECURITY,
            },
            "mails" : [{
                "email"         : "u@test.com",
                "user_id"       : "u",
                "name"          : "u"  + ' ' + "last",
                "password"      : "test",
                },
                {
                "email"         : "u@test.com",
                "user_id"       : "u",
                "name"          : "u"  + ' ' + "last",
                "password"      : "test",
                }
                ]
            }
        return expected_data


    def test_create_json(self):
        dd_data = [{
            "email"         : "u@test.com",
            "user_id"       : "u",
            "name"          : "u"  + ' ' + "last",
            "password"      : "test",
            },
            {
            "email"         : "u@test.com",
            "user_id"       : "u",
            "name"          : "u"  + ' ' + "last",
            "password"      : "test",
            }
            ]
        json_data = utils.build_users_mail_josn(dd_data)
        print("===== Running TEST: test_create_json =====")
        self.assertEqual(json_data, json.dumps(self.get_expected_data()))
    def test_get_own_user(self):
        get_url = settings.SITE_URL + "api/users/"
        json_res = utils.get_api_jws_data(get_url)
        print("===== Running TEST: test_get_user =====")
        self.assertEqual(json_res, {'message': 'Hola mundo'} )

    def test_post_self(self):
        users = self.get_expected_data()
        json_users = json.dumps(users)
        encoded = utils.encrypt_jwe(json_users)
        get_url = settings.SITE_URL + "api/users/"
        users_add = utils.post_changes_to_api(encoded, get_url)
        self.assertEqual(users_add, {'message': 'Hola mundo'} )

    def test_delete_account(self):
        user=[{
	  "id": "man2",
	  "first": "man",
	  "last": "2",
	  "role": "manager",
	  "email": "man2@work.dd-work.space",
	  "groups": [
	   "grup1",
	   "manager"
	  ],
	  "quota": "500 MB"
	 },
         ]

        data = json.dumps(user)
        ldap = Ldap()
        users = json.loads(data)
        for user in users:
            ldap.delete_mail_account(user)
