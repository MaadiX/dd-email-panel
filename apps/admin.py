"""
class LdapDomainAdmin(admin.ModelAdmin):
    list_display = ['name', 'adminID']

admin.site.register(models.LdapDomain, LdapDomainAdmin)
"""
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group

from aoocunt.models import Role

#from .forms import PeopleCreationForm, PeopleChangeForm
from .models import User, School 

admin.site.register(User, UserAdmin)

class SchoolAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'domain'
    )
    search_fields = (
        'name',
        'domain'
    )

    list_filter  = ('domain',)
admin.site.register(School,SchoolAdmin)


admin.site.unregister(Group)
admin.site.register(Role, GeoupAdmin)
