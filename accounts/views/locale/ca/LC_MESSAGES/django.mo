��          t      �              8   -     f     �     �  %   �     �  #     +   &  =   R  B  �     �  5   �     %     @     ]     v     �  @   �  )   �  9                 
                            	                  %s no es un correo válido. Contraseña de la cuenta de correo modificada con éxito Cuenta  %s añadida con éxito Cuenta  %s eliminada con éxito La Cuenta  %s no existe La cuenta %s no es un correo válido. No hay ninguna cuenta todavía Please fill out the form correctly. Respuesta automática modificada con éxito Se ha producido un error. La Contraseña no se ha modificado. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %s no és un correu vàlid. Contrasenya del compte de correu modificada amb èxit Compte %s afegit amb èxit Compte %s eliminat amb èxit El Compte %s no existeix %s no és un correu vàlid. No hi ha cap compte encara Si us plau, emplena el tots els camps del formulari correctament Resposta automàtica modificada amb èxit S'ha produït un error. La Contrasenya no s'ha modificat. 