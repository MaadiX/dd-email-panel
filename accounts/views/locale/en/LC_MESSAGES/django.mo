��          |      �             !  8   =     v     �     �  %   �     �  #     )   6  +   `  =   �  B  �       +   &     R     p     �  $   �     �  #   �  *     (   <  5   e                               
          	                  %s no es un correo válido. Contraseña de la cuenta de correo modificada con éxito Cuenta  %s añadida con éxito Cuenta  %s eliminada con éxito La Cuenta  %s no existe La cuenta %s no es un correo válido. No hay ninguna cuenta todavía Please fill out the form correctly. Renvío automático modificado con éxito Respuesta automática modificada con éxito Se ha producido un error. La Contraseña no se ha modificado. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %s is not a valid email. Email account password successfully changed Account %s successfully added Account %s successfully deleted The %s account does not exist The %s account is not a valid email. There are no accounts yet Please fill out the form correctly. Successfully modified automatic forwarding Successfully modified automatic response An error has occurred. Password has not been changed. 