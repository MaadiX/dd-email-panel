import json, sys, requests, os
import logging
import email.utils
from urllib.error import HTTPError
from django import views
from django.shortcuts import render
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.contrib.auth.models import Group, Permission
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse, reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.contrib.postgres.fields import ArrayField
# project
sys.path.append('../api')
from api import utils
from api.ldap import utils as ldaputils
from api.ldap.LdapBackend import Ldap
#from accounts.forms import ImportUsersForm
from django.conf import settings
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from accounts.models import Account
sys.path.append('../api')
from api import utils
from api.ldap.LdapBackend import Ldap
from api.ApiBackend  import ApiBackend

logger = logging.getLogger("accounts")

@csrf_exempt
@utils.is_api_user
def manage_users(request):
    if request.method in ["POST", "PUT", "DELETE"]:
        ldap = Ldap()
        data = request.body
        key_obj = ApiBackend()
        users = key_obj.key_manager.verify_and_decrypt_incoming_json(request.body)

        if request.method == 'POST' or request.method == 'PUT':
            if users:
                messages = ldap.add_user_account(users)
                # TODO:Handle error - notify dd?
                """
                if messages["error"]:
                    print(messages["error"])
                """
            return JsonResponse({'message': 'Creating user'})

        elif request.method == 'DELETE':
            for user in users:
                ldap.delete_mail_account(user)
            return JsonResponse({'message': 'Deleteing user'})
