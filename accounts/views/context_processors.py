# django
from django.utils.text import slugify
# project
from django.conf import settings
from accounts.models import Account

def active_section_processor(request):
    """Injects into global context the slug of the path as an section identifier"""
    # Check if django is runnung in subpath
    active_section = slugify(request.path.lstrip('/'))
    return locals()

def school_mail_processor(request):
    user_id = request.user.id
    try:
        account = Account.objects.get(id=user_id )
        user_mail = account.email
        school_mail_domain  = settings.SCHOOL_MAIL_DOMAIN
        if(user_mail.split("@")[1].strip() == school_mail_domain.strip()):
            school_mail=True
        else:
            school_mail=False
    except Exception as e:
        school_mail = False
    return locals()

def school_domain_processor(request):
    school_domain = settings.SCHOOL_DOMAIN 
    return locals()
