import json, sys, requests, os, base64
import logging
import functools
import email.utils
from urllib.error import HTTPError
from django import views
from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.safestring import mark_safe
from django.shortcuts import render
from django.core import serializers
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.views.generic.edit import FormView, FormMixin, ProcessFormView
from django.views.generic.base import TemplateView
from django.views.generic import ListView
from django.template.response import TemplateResponse
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse, reverse_lazy
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django_tables2 import SingleTableView
from accounts.tables import AccountTable, WhiteListTable, AllowedDomainsTable
from django.contrib.postgres.fields import ArrayField
from operator import and_
from jose import jwt, jws, jwk, jwe
from jose.backends.rsa_backend import RSAKey
from jose.backends.base import DIRKey
from jose.utils import base64url_decode
from cryptography.hazmat.primitives import serialization
# project
sys.path.append('../api')
from api import utils
from api.ApiBackend  import ApiBackend
from api.ldap.LdapBackend import Ldap
from api.ldap import utils as ldaputils
#from accounts.forms import ImportUsersForm
from django.conf import settings
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from accounts.models import Account 
from accounts.forms import UserCreationForm, ChangePassordForm ,ForowardEditForm, VacationEditForm, EditAccountForm, WhiteListMassRemoval,WhiteListMassSubscription
logger = logging.getLogger("accounts")

""" Frontend Views and Forms """
def LoginRedirect(request):
    # Custom landing url to redirect users after login according to their roleº
    # With 2FA the LoginView is provided by two_factor
    if request.user.is_authenticated:
        """ Redirection url if form is valid. Depends on user's role """
        return HttpResponseRedirect(reverse('profile'))
    else:
        return HttpResponseRedirect(reverse('saml2_login'))

class ProfileView(PermissionRequiredMixin,views.View):
    template_name = "accounts/view-profile.html" 
    permission_required = 'accounts.can_edit_own_account'
    
    def get(self, request,  **kwargs):
        user_id = request.user.id
        account = Account.objects.get(id=user_id )
        try:
            ldap = Ldap()
            user = ldap.ldap_get_mail_data(account.email)
        except Exception as e:
            msg = "Unsuccessful read  operation: {e}".format(e=e)
            logger.error(msg)
        finally:
            ldap.unbind()
        return render(request, self.template_name, locals())

class EmailClientView(LoginRequiredMixin,views.View):
    """ Show details For cnfiguring email client"""
    
    template_name = "accounts/email-client.html"

    def get(self, request,  **kwargs):
        user_id = request.user.id
        account = Account.objects.get(id=user_id )
        group_permissions = Permission.objects.filter(group__user=account)     
        mail_account = account.email
        smpt_server  = settings.SMTP_SERVER
        smtp_port    = settings.SMTP_PORT
        imap_server  = settings.IMAP_SERVER
        imap_port    = settings.IMAP_PORT
        pop3_server  = settings.POP3_SERVER
        pop3_port    = settings.POP3_PORT
        auth_pass    = settings.AUTH_PASS
        security     = settings.MAIL_SECURITY
        return render(request, self.template_name, locals())


""" generic class for user forms """
class GenericSelfEditView(PermissionRequiredMixin, FormView):

    permission_required = 'accounts.can_edit_own_account'
    template_name = "accounts/edit-profile.html" #your_template

    # Override this method if we are editing
    # a user that is not us
    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_user(self):
        user_id = self.request.user.id
        account = Account.objects.get(id=user_id )
        return account

    def get_email(self, **kwargs):
        user_id = self.request.user.id
        account = Account.objects.get(id=user_id )
        return account.email

    def get_context_data(self, **kwargs):
        context = super(GenericSelfEditView, self).get_context_data(**kwargs)
        context['mail_account'] = self.get_email()
        return context


class ChangePasswordView(GenericSelfEditView):
    """ This class only allows to change own password """
    form_class = ChangePassordForm
    mail_account    = ''
    ldap = Ldap()


    def form_valid(self, form):
        """ Writing in ldap uses the dn returned by the ldapsearch.
        If the url parameter is changed, the dn will be the previous, 
        and it must exists
        """
        mail_account =  self.get_email()
        domain = mail_account.split('@')[1]
        dn='mail=%s,vd=%s,%s' % (mail_account,domain,settings.LDAP_TREE_HOSTING)
        if 'password' in form.fields and form['password'].value():
            pwd = form['password'].value()
            account = Account.objects.get(email=mail_account )

            try:
                api = ApiBackend()
                self.ldap.modify_pass(dn,pwd)
                # Nedd to update password to notifiy DD
                dd_data = [{
                    "email"         : mail_account,
                    "user_id"       : account.username,
                    "name"          : '%s %s' % (account.first_name, account.last_name),
                    "password"      : pwd,
                    "outbound_user" : mail_account,
                }]
                send_data = {
                    "config"   : utils.mail_server_config(),
                    "users" : dd_data,
                }
                # Notify DD updan password change
                api.post_changes_to_api(send_data)
                self.ldap.unbind()
                messages.success(self.request, _('Contraseña de la cuenta de correo modificada con éxito'))
            except Exception as e:
                logging.error("Could not update password " , e)
                messages.error(self.request, _('Se ha producido un error. La Contraseña no se ha modificado.'))
        return HttpResponseRedirect(self.get_success_url())


class EditForwardView(GenericSelfEditView):
    form_class = ForowardEditForm
    ldap            = {}
    mail_account    = ''
    error = ''
    ldap_attributes = [
        'givenName',
        'sn',
        'forwardActive',
        'maildrop',
    ]

    def get_form(self):
        not_permitted=[]
        """Return an instance of the form to be used in this view."""
        self.mail_account =  self.get_email()
        domain = self.mail_account.split('@')[1]
        dn=dn='mail=%s,vd=%s,%s' % (self.mail_account,domain,settings.LDAP_TREE_HOSTING) 
        try:
            ldap = Ldap()
            account = ldap.search(
                dn,
                '(objectClass=VirtualMailAccount)',
                self.ldap_attributes
                )
            kwargs   = self.get_form_kwargs()
            kwargs['initial'] = { k : ldaputils.ldap_val(account[k].value) for k in self.ldap_attributes }
            # We want to show only recipients that do not match with own email
            # as it is showned by the checkbox
            #if str(self.mail_account) in str(account.maildrop):
            mails = str(account.maildrop)
            split_line = mails.split(",")
            for w in split_line:
                if str(w)==str(self.mail_account):
                    split_line.remove(self.mail_account)
                else:
                    print("DRDRDRDRDR ", w)
                    if w.split('@')[1] != domain:
                        dn = "mail=%s,%s" % (w,settings.LDAP_WHITELIST_BASE)
                        try:
                            permitted = ldap.search(
                                dn,
                                settings.LDAP_FILTERS_WITELIST,
                                ["mail"]
                            )
                            if not permitted:
                                not_permitted.append(w)
                        except Exception as e:
                            pass
            # TODO: notify in template that there are some not permitted recipients
            user = self.request.user
            if user.groups.filter(name='student').exists():
                msg_action = _("Solicita al centro que la añadan.")
            else:
                msg_action = _("Puedes añadirla desde la pestaña  Destinatarios permitidos -> Cuentas de correo -> Añadir.")
            if not_permitted:
                messages.info(self.request, _('No se pueden renviar mails a %(dest)s porque no está entré los correos permitidos. %(action)s ' % ({"dest" : not_permitted, "action" :msg_action})))
            maildrop = ','.join(split_line)
            kwargs['initial']['maildrop'] = maildrop
            kwargs['initial']['sendme']= True
            return ForowardEditForm(mail_account=self.mail_account,**kwargs)
        except Exception as e:
                print(e)
        return ForowardEditForm(mail_account=self.mail_account,**self.get_form_kwargs())

    def form_valid(self, form):

        """ Writing in ldap uses the dn returned by the ldapsearch.
        If the url parameter is changed, the dn will be the previous, 
        adef post(self, request, *args, **kwargs):nd it must exists
        """
        form.save()
        messages.success(self.request, _('Reenvío automático modificado con éxito'))
        return HttpResponseRedirect(self.get_success_url())

class EditVacationView(GenericSelfEditView):
    form_class = VacationEditForm
    ldap            = {}
    mail_account    = ''
    error = ''
    ldap_attributes = [
        'givenName',
        'sn',
        'vacationActive',
        'vacationinfo',
    ]


    def get_form(self):
        self.mail_account =  self.get_email()
        domain = self.mail_account.split('@')[1]
        dn='mail=%s,vd=%s,%s' % (self.mail_account,domain,settings.LDAP_TREE_HOSTING)
        try:
            ldap = Ldap()
            account = ldap.search(
                dn,
                '(objectClass=VirtualMailAccount)',
                self.ldap_attributes
            )
            kwargs    = self.get_form_kwargs()
            kwargs['initial'] = { k : ldaputils.ldap_val(account[k].value) for k in self.ldap_attributes }
            return self.form_class(mail_account=self.mail_account,**kwargs)
        except Exception as e:
                print(e)
        return self.form_class(mail_account=self.mail_account,**self.get_form_kwargs())

    def form_valid(self, form):

        """ Writing in ldap uses the dn returned by the ldapsearch.
        If the url parameter is changed, the dn will be the previous, 
        and it must exists
        """
        msg = form.save()
        if msg:
            messages.error(self.request, msg)
            return super().form_invalid(form)
            #return self.form_class(mail_account=self.mail_account,**kwargs)
        else:
            messages.success(self.request, _('Respuesta automática modificada con éxito'))
            return HttpResponseRedirect(self.get_success_url())

class ListUsersView(PermissionRequiredMixin,SingleTableView):
    """ This view will list all students according to 
        User privileges.
        If Teacher, will only access users in their own groups
        If Manager, can access all students in school
    """
    permission_required = 'accounts.can_edit_student_account'
    model = Account 
    table_class = AccountTable
    template_name = 'accounts/account_list.html' 

    def get_queryset(self):
       filterMail = "@%s" % settings.SCHOOL_MAIL_DOMAIN 
       users = Account.objects.all().filter(email__contains=filterMail).exclude(is_superuser=True)
       query = self.request.GET.get("q")

       if query:
          users = Account.objects.filter(
            Q(username__icontains=query) |
            Q(email__icontains=query) |
            Q(groups__name=query) |
            Q(tags__contains=[query])
          )
       return users 

class AccountProfileView(ProfileView):
    template_name = "accounts/view-account.html"
    permission_required = 'accounts.can_edit_student_account'
    ldap = Ldap()
    # This a view ofr editing another account
    # So get it from pk
    def get(self, request,  **kwargs):
        user_id = kwargs['pk']
        account = Account.objects.get(id=user_id )
        try:
            user = self.ldap.ldap_get_mail_data(account.email)
        except Exception as e:
            msg = "Unsuccessful read  operation: {e}".format(e=e)
            logger.error(msg)
        #return render(request,'index.html',{'response':response})
        return render(request, self.template_name, locals())


class AccountPasswordView(ChangePasswordView):
    template_name = "accounts/edit-account.html"
    permission_required = 'accounts.can_edit_student_account'

    # This a view ofr editing another account
    # So get it from pk

    def get_email(self, **kwargs):
        user_id =  self.kwargs['pk']
        account = Account.objects.get(id=user_id )
        return account.email

    def get_context_data(self, **kwargs):
        context = super(GenericSelfEditView, self).get_context_data(**kwargs)
        context['mail_account'] = self.get_email()
        context['user_id'] = self.kwargs['pk']
        return context

class AccountForwardView(EditForwardView):
    template_name = "accounts/edit-account.html"
    permission_required = 'accounts.can_edit_student_account'

    # This a view ofr editing another account
    # So get it from pk

    def get_email(self, **kwargs):
        user_id =  self.kwargs['pk']
        account = Account.objects.get(id=user_id )
        return account.email

    def get_context_data(self, **kwargs):
        context = super(GenericSelfEditView, self).get_context_data(**kwargs)
        context['mail_account'] = self.get_email()
        context['user_id'] = self.kwargs['pk']
        return context

class AccountVacationView(EditVacationView):
    template_name = "accounts/edit-account.html"
    permission_required = 'accounts.can_edit_student_account'

    # This a view ofr editing another account
    # So get it from pk


    def get_email(self, **kwargs):
        user_id =  self.kwargs['pk']
        account = Account.objects.get(id=user_id )
        return account.email

    def get_context_data(self, **kwargs):
        context = super(GenericSelfEditView, self).get_context_data(**kwargs)
        context['mail_account'] = self.get_email()
        context['user_id'] = self.kwargs['pk']
        return context

class AllowedDomainsView(PermissionRequiredMixin,SingleTableView):
    permission_required = 'accounts.can_edit_own_account'
    table_class = AllowedDomainsTable
    template_name = 'accounts/allowed_domain_list.html'
    # get a list of allowed domains under:
    #o=domains,dc=whitelist,dc=tld

    attrs={'mail' }
    ldap = Ldap()

    def get(self, request,  **kwargs):
        data =[]
        query = self.request.GET.get("q")
        filter_query = '(objectClass=inetOrgPerson)'
        if query:
            filter_query = '(&(objectClass=inetOrgPerson)(mail=*%(query)s*))' % {'query': query}
        domains = self.ldap.get_allowed_domains(filter_query)
         
        if domains["domains"] or query:
            for domain in domains["domains"]:
                data.append({k : ldaputils.ldap_val(domain[k].value) for k in self.attrs })
            table = self.table_class(data)
            return render(request, self.template_name, {
                "table": table
            })
        elif domains["message"]:
            return render(request, self.template_name, {
                "message":  domains["message"]
            })

        else:
            return render(request, self.template_name, {
                "message": _('No hay ningún dominio en la lista')
            })



class WhiteListUsersView(PermissionRequiredMixin,SingleTableView):
    """ This view will list all students according to 
        User privileges.
        If Teacher, will only access users in their own groups
        If Manager, can access all students in school
    """
    permission_required = 'accounts.can_edit_whitelist'
    #model = Account
    table_class = WhiteListTable 
    template_name = 'accounts/whitelist_list.html'
    # get a list of whitelisted emails under:
    #o=ESCUELA,o=mails,dc=whitelist,dc=tld
    #this tree is common for the whole project
    # so school must be a unique ID 

    attrs={'mail', 'sn'}
    ldap = Ldap()

    def get(self, request,  **kwargs):
        # connecting to the whitelist trees
        # Credentials
        #AUTH_LDAP_ADMIN_BIND_DN 
        #AUTH_LDAP_BIND_ADMIN_PASSWORD 
        # tree
        #LDAP_WHITELIST_BASE  = "o=mails,dc=whitelist,dc=tld"
        #dn = 'mail=s%,%s,%d,%s'z 
        #dn: mail=e489f426d1666d685557@38af1f8c37069087cd88.6dc,o=mails,dc=whitelist,dc=tld
        #cn: d
        #sn: d
        #objectClass: top
        #objectClass: inetOrgPerson
        #mail: e489f426d1666d685557@38af1f8c37069087cd88.6dc
        school_name = settings.SCHOOL_MAIL_DOMAIN
        data =[] 
        query = self.request.GET.get("q")
        filter_query = '(destinationindicator=%s)' % settings.IMAP_SERVER
        if query:
            filter_query = '(|(&%(filter)s(sn=*%(query)s*))(&%(filter)s(mail=*%(query)s*)))' % {'filter' : filter_query, 'query': query}
            #filter_query = '(|(&(objectClass=inetOrgPerson)(sn=*%(query)s*))(&(objectClass=inetOrgPerson)(mail=*%(query)s*)))' % {'filter_query': filter_query, 'query': query}
        emails = self.ldap.get_whiltelisted_emails(filter_query)

        if emails["emails"] or query:
            for mail in emails["emails"]: 
                data.append({k : ldaputils.ldap_val(mail[k].value) for k in self.attrs })
            table = WhiteListTable(data)
            return render(request, self.template_name, {
                "table": table
            })
        elif emails["message"]:
            return render(request, self.template_name, {
                "message":  emails["message"]
            }) 

        else: 
            return render(request, self.template_name, {
                "message": _('No hay ninguna cuenta todavía')
            }) 
class WhiteListMassAddView(PermissionRequiredMixin, FormView):
    """Class For Mass Removal"""

    permission_required = 'accounts.can_edit_whitelist'
    form_class = WhiteListMassSubscription
    template_name = 'accounts/whitelist-add.html'
    ldap = Ldap()
    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def post(self, request, *args, **kwargs):
        form = WhiteListMassRemoval(request.POST)
        if not form.is_valid():
            messages.error(request, _('Por favor, rellena todos los campos como requerido.'))
        else:
            school_name = settings.SCHOOL_MAIL_DOMAIN
            # First check if main tree exist or create it
            #ldaputils.get_or_create_basetree(ldap, school_name)
            for data in form.cleaned_data['emails']:
                try:
                    display_name, address = email.utils.parseaddr(data)
                    validate_email(address)
                    # Add entry here
                    new_account = self.ldap.add_whitelisted_email(address, display_name)
                    if new_account:
                        messages.success(request, _('Cuenta  %s añadida con éxito') % address)
                    else:
                        messages.error(request, _('La cuenta de correo %s no se ha podido añadir') % address)
                except (HTTPError, ValueError) as e:
                    messages.error(request, e)
                except ValidationError:
                    messages.error(request, _('%s no es un correo válido.') % address)

        return HttpResponseRedirect(self.get_success_url())

class WhiteListMassRemoveView(PermissionRequiredMixin, FormView):
    permission_required = 'accounts.can_edit_whitelist'
    #model = Account
    form_class = WhiteListMassRemoval 
    template_name = 'accounts/whitelist-remove.html'
    ldap = Ldap()
    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if not form.is_valid():
            messages.error(request, _('Please fill out the form correctly.'))
        else:
            school_name = settings.SCHOOL_MAIL_DOMAIN
            # First check if main tree exist or create it
            for address in form.cleaned_data['emails']:
                try:
                    validate_email(address)
                    # Add entry here
                    result = self.ldap.remove_whitelisted_email(address.lower())
                    if result["result"] == 32:
                        messages.error(request, _('La Cuenta  %s no existe') % address)
                    else:
                        messages.success(request, _('Cuenta  %s eliminada con éxito') % address)
                except (HTTPError, ValueError) as e:
                    messages.error(request, e)
                except ValidationError:
                    messages.error(request, _('La cuenta %s'
                                              ' no es un correo válido.') % address)
        return HttpResponseRedirect(self.get_success_url())

def pub_key_view(request):
    #key = utils.get_pub_key(settings.RSA_PUB_KEY_FILE)
    #rsa_pub = settings.RSA_PUB_KEY_FILE
    #rsa_priv = settings.RSA_PRIV_KEY_FILE
    key_obj = ApiBackend()
    key = key_obj.key_manager.our_pubkey_jwk
    return JsonResponse(key)
