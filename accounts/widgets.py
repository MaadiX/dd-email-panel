# django
from django.forms.widgets import CheckboxInput, TextInput, Widget
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.utils.encoding import smart_text
from django import forms

class LabelledCheckbox(CheckboxInput):
    """A custom checkbox that displays a label besides the checkbox """

    def __init__(self, attrs=None, label=_('Activado')):
        self.label = label
        super(LabelledCheckbox, self).__init__(attrs)

    def render(self, name, value, attrs=None,renderer=None):
        """Render widget"""
        parent_widget = super(LabelledCheckbox, self).render(name, value, attrs )
        widget = render_to_string("blocks/labelled-checkbox.html", {
            'parent_widget' : parent_widget,
            'label'         : self.label,
        })
        return widget


class ListTextWidget(TextInput):
    """ A custom widget that lets the user to select a existing value or add a new value. """
    """ @see https://stackoverflow.com/questions/24783275/django-form-with-choices-but-also-with-freetext-option """

    def __init__(self, data_list, name, *args, **kwargs):
        super(ListTextWidget, self).__init__(*args, **kwargs)
        self._name = name
        self._list = data_list
        self.attrs.update({ 'list' : 'list__%s' % self._name })

    def render(self, name, value, attrs=None, renderer=None):
        text_html = super(ListTextWidget, self).render(name, value, attrs=attrs)
        data_list = '<datalist id="list__%s">' % self._name
        for item in self._list:
            data_list += '<option value="%s">%s' % (item,item)
        data_list += '</datalist>'

        return (text_html + data_list)

class HeaderWidget(Widget):
    def __init__(self, attrs=None, label=None, tag='h1'):
        self.label = label
        self.tag = tag
        super(HeaderWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None,renderer=None):
        
        widget = render_to_string("blocks/header-form-field.html", {
            'tag'   : self.tag,
            'label' : self.label,
            })
        return widget

# From postorius code, for addint bulk emails
class ListOfStringsField(forms.Field):
    widget = forms.widgets.Textarea

    def prepare_value(self, value):
        if isinstance(value, list):
            value = '\n'.join(value)
        return value

    def to_python(self, value):
        "Returns a list of Unicode object."
        if value in self.empty_values:
            return []
        result = []
        for line in value.splitlines():
            line = line.strip()
            if not line:
                continue
            result.append(smart_text(line))
        return result

