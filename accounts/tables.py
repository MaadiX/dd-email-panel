import django_tables2 as tables
from accounts.models import Account 
from django.utils.html import format_html
from django_tables2.utils import A
import itertools
class AccountTable(tables.Table):
    #row_number = tables.Column(empty_values=())
    roles = tables.Column(orderable=False, empty_values=())
    edit = tables.LinkColumn(
        "account-profile",
        #text=lambda record: record.username,
        text=format_html("<i class='fa fa-gear'></i> / <i class='fa fa-eye'></i>"),
        args=[A("pk")],
        orderable=False)

    class Meta:
        model = Account 
        row_number = tables.Column(empty_values=())
        template_name = "django_tables2/bootstrap.html"
        fields = ("username","email","tags" )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count()
    """
    def render_row_number(self):
        return "Row %d" % next(self.counter)
    """
    def render_tags(self, value):
        all_groups=""
        for item in value: 
            all_groups += ', ' + item
        return format_html("{}", all_groups.strip(','))

    def render_roles(self, record):
        groups = record.groups.all()
        for item in groups: 
            return format_html("{}", item)

class WhiteListTable(tables.Table):

    N = tables.Column(empty_values=())
    mail = tables.Column()
    sn = tables.Column()


    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.counter = itertools.count()


    def render_N(self):

        return "%d" % next(self.counter)


    def render_id(self, value):

        return "<%s>" % value

    class Meta:
        template_name = "django_tables2/bootstrap.html"

class AllowedDomainsTable(tables.Table):

    N = tables.Column(empty_values=())
    mail = tables.Column()


    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.counter = itertools.count()


    def render_N(self):

        return "%d" % next(self.counter)


    def render_id(self, value):

        return "<%s>" % value

    class Meta:
        template_name = "django_tables2/bootstrap.html"

