# python
import os, socket, time
# django
from django import template
from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.safestring import mark_safe
# project
from django.conf import settings
from datetime import datetime
import time

register = template.Library()

@register.simple_tag
def adminjs(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_ADMIN_FOLDER + '/js/' + file

@register.simple_tag
def css(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/css/' + file

@register.simple_tag
def js(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/js/' + file

@register.simple_tag
def img(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/img/' + file

@register.simple_tag
def ddcss(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/dd/css/' + file

@register.simple_tag
def ddjs(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/js/' + file

@register.simple_tag
def ddimg(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/img/' + file

@register.filter(name='has_group') 
def has_group(account, group_name):
    return account.groups.filter(name=group_name).exists() 

@register.filter('startswith')
def startswith(text, starts):
    if isinstance(text, str):
        return text.startswith(starts)
    return False

@register.filter
def tobytes(value):
    if value:
        return int(int(value.value) / (1024**2))
    else:
        return None

@register.filter
def path_exists(appname):
    result = False
    if appname == 'mailman':
        path = '/opt/mailman'
    if os.path.exists(path):
        result = True
    return result
