import json, sys, requests, os, logging
from django.core.management.base import BaseCommand, CommandError
#from accounts.forms import ImportUsersForm
from django.conf import settings
# project
sys.path.append('../api')
from api import utils
from api.ApiBackend  import ApiBackend

#Models
logger = logging.getLogger("accounts")

class Command(BaseCommand):
    help = 'Get a list of available users from api defined in local_settings as SCHOOL_API_URL: %s \n' % settings.SCHOOL_API_URL

    def handle(self, *args, **options):
        users = None
        try:
            api = ApiBackend()
            users= api.get_api_users()
            if users:
                self.userlist = json.dumps(users,indent=1)
                result = self.style.SUCCESS('Successfully connected to api. These are the available users %s\n' % self.userlist )
            else:
                result = self.style.ERROR('Error connecting to api s %s' % connection)
        except Exception as e:
            logger.debug("Error connecting to api {}".format(e))
            result = self.style.ERROR('Error s %s' % e)

        self.stdout.write(result)
