import json, sys, requests, os, logging
from django.core.management.base import BaseCommand, CommandError
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
# project
sys.path.append('../api')
from api import utils
from api.ldap import utils as ldaputils
from api.ldap.LdapBackend import Ldap
from api.ApiBackend  import ApiBackend
#Models
logger = logging.getLogger("accounts")

class Command(BaseCommand):
    help = 'Import users from api defined in local_settings as SCHOOL_API_URL: %s \n' % settings.SCHOOL_API_URL
    help+=        'You can previously check a list of users that will be imported by running \n'
    help+=        'python3 manage.py getapiusers' 

    def handle(self, *args, **options):
        self.stdout.ending = '\n'
        ## DD is asking for all smpt data for each user
        # We build the dict to be sent 
        # TODO_ APi is going to change endpoints. Update wher reaady
        key_manager = ApiBackend()

        users = key_manager.get_api_users()
        if users:
            ldap = Ldap()
            messages = ldap.add_user_account(users["users"])
        #Once we have imported all users, send post to dd 
        # If there were some new user, notify api
        if messages["update_message"]:
            self.stdout.write(self.style.SUCCESS('Successfully updates users %s \n' % messages["update_message"]))
        if messages["success_msg"]:
            self.stdout.write(self.style.SUCCESS('Successfully imported users %s' % messages["success_msg"] ))
        else:
            self.stdout.write(self.style.SUCCESS('No new users to import \n'))
        if messages["errors"]:
            self.stdout.write(self.style.ERROR('Error importing users %s' % messages["errors"]))
        if messages["info_msg"]:
            self.stdout.write(self.style.ERROR("Dominio no válido para crear las cuentas de correo :\n %s" % messages["info_msg"]))
