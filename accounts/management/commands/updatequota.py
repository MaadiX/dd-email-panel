import json, sys, requests, os, logging
from django.core.management.base import BaseCommand, CommandError
from ldap3.utils.hashed import hashed
from django.shortcuts import get_object_or_404
from django.conf import settings
# project
sys.path.append('../api')
from api import utils
from api.ApiBackend  import ApiBackend
from api.ldap import utils as ldaputils
from api.ldap.LdapBackend import Ldap
from accounts.models import Account


#Models
logger = logging.getLogger("accounts")

class Command(BaseCommand):
    help = 'Update extisting users'

    def handle(self, *args, **options):
        users = None
        quota = settings.DEFAULT_QUOTA
        # DATA is a list of dict with data for the Account Model objects
        data = {}
        #nc_data is to build the json to send to the api
        nc_data = []

        try:
            ldap = Ldap()
            emails = ldap.get_all_emails()
            if emails:
                for user in emails:
                    print("PROCESSSING USER ", user.cn)
                    data = {
                        "quota"         : quota
                    }

                    dn = user.entry_dn
                    ldap.modify(dn,data)
            result = self.style.SUCCESS('Operation performed succesfully')
        except Exception as e:
            logger.debug("Error updating some users {}".format(e))
            result = self.style.ERROR('Error s %s' % e)

        self.stdout.write(result)
