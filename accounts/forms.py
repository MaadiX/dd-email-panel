from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.password_validation import validate_password
from django.contrib import messages
from django.core.validators import validate_email
from django.conf import settings
from django.utils.safestring import mark_safe
from ckeditor.widgets import CKEditorWidget
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from crispy_forms.helper import FormHelper
from .models import Account
from . import widgets, customfileds
import sys, logging

logger = logging.getLogger("accounts")

sys.path.append('../api')
from api import utils
from api.ldap import utils as ldaputils
from api.ldap.LdapBackend import Ldap

class RegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    class Meta:
        model = Account
        fields = ('username', 'email', 'first_name', 'last_name', 'tags', 'mailpassword', 'password')

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Account
        fields = ('username', 'email', 'first_name', 'last_name', 'tags', 'mailpassword', 'is_staff', 'is_superuser')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Account
        fields = ('username', 'email', 'first_name', 'last_name', 'tags', 'mailpassword', 'password', 'is_active', 'is_superuser')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class CustomDivForm(forms.Form):
    helper = FormHelper()

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )

class GenericPassForm(CustomDivForm):
    """
    Generic form to be inherited by regular forms on the site.
    """

    required_css_class = 'required'
    error_css_class    = 'error'

    #  Clean method that check if passwords coincide
    #  In case the form doesn't have both password fields
    #  it will work because both fields will be None
    def clean(self):
        cleaned_data = super(GenericPassForm, self).clean()
        password           = cleaned_data.get("password")
        confirmed_password = cleaned_data.get("password_2")
        if password and not confirmed_password:
            raise forms.ValidationError(
                _("Por favor confirma la contraseña que has introducido.")
            )
            context['display_form'] = True
        if password != confirmed_password:
            raise forms.ValidationError(
                _("Las contraseñas no coinciden.")
            )
            context['display_form'] = True
        if password:
            validate_password(password)

class ChangePassordForm(GenericPassForm):

    password       = forms.CharField(label= _('Password') , widget=forms.PasswordInput(),
                                         required=False)
    password_2     = forms.CharField(label=_('Please Confirm Password'),
                                         widget=forms.PasswordInput(), required=False)
    def __init__(self, *args, **kwargs):

        self.get_email = kwargs.pop('mail_account', None)
        super(ChangePassordForm,self).__init__(*args, **kwargs)

    def clean(self):
        super().clean()
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )


class ForowardEditForm(CustomDivForm):
    forward_help_text  = _("Puedes reenviar los correos electrónicos entrantes a "
                     "una o más cuentas de correo alternativas. Si quieres "
                     "que se reenvíen a múltiples cuentas, separa cada una "
                     "de ellas con una coma (user1@example.com,user2@example.com. ")
    forwardActive  = forms.BooleanField(label=_("Activar reenvío automático"), required=False,)
    maildrop       = forms.CharField(label=_('Cuentas de destino para el reenvío automático'),
                                         help_text=forward_help_text,widget=forms.Textarea, required=False)

    sendme = forms.BooleanField(label=_("Recibir una copia a mi buzón"), required=False)

    def clean(self):
        super().clean()
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
        forward_active = self.cleaned_data.get("forwardActive")
        maildrop = self.cleaned_data.get("maildrop")
        if(forward_active and not maildrop):
            raise forms.ValidationError(
                _("Para activar el reenvío automático tienes que insertar al menos una cuenta de correo electrónico")
            )
        if (forward_active):
            emails   = maildrop.split(",")
            for email in emails:
                email = email.strip()
                """
                if email and not re.match("[^@]+@[^@]+\.[^@]+", email):
                    raise forms.ValidationError(
                        _("Parece que uno de los correos que has introducido no es válido.")
                    )
                """
                # Check forward email format with django builin
                validate_email(email)
    def __init__(self, *args, **kwargs):
        self.get_email = kwargs.pop('mail_account')
        super(ForowardEditForm,self).__init__(*args, **kwargs)
        self.fields['forwardActive'].widget.attrs = { 'data-link-display' : 'id_maildrop' }

    def save(self):
        msg =None
        mail_account = self.get_email 
        domain = mail_account.split('@')[1]
        dn='mail=%s,vd=%s,%s' % (mail_account,domain,settings.LDAP_TREE_HOSTING)
        forward = ldaputils.ldap_bool(self.cleaned_data["forwardActive"])
        maildrop = self.cleaned_data["maildrop"]
        sendme = self.cleaned_data["sendme"]
        if (sendme and str(mail_account) not in str(maildrop)):
            maildrop +=','+ mail_account
        elif( not sendme and str(mail_account) in str(maildrop)):
            print("jhjkhhljl")
            split_line = maildrop.split(",")
            for w in split_line:
                if str(w)==str(mail_account):
                    split_line.remove(mail_account)
            maildrop = ','.join(split_line)
        try:
            ldap = Ldap()
            data = {
                    'forwardActive' : forward,
                    'maildrop'  : maildrop
                    }

            ldap.modify(dn,data) 
            # Nedd to update password in Django, tp notifiy DD
            # TODO: How do we stroe new new pass in django postrge?????
            ldap.unbind()
        except Exception as e:
            msg = "Error adding ldap entry: {}".format(e)
            logger.error(msg)
            raise forms.ValidationError(
             _('Se ha producido un error configurando el reenvío automático')
             )
        return msg

class VacationEditForm(CustomDivForm):

    vacationActive = forms.BooleanField(label=_("Activar respuesta automática"), required=False,)
    vacationinfo   = forms.CharField(label=_('Mensaje de respuesta automática'), required=False,
                                        widget=CKEditorWidget(),
                                        help_text=_('Inserta el texto del mensaje de respuesta automática.'))
    def __init__(self, *args, **kwargs):
        self.get_email = kwargs.pop('mail_account')
        super(VacationEditForm,self).__init__(*args, **kwargs)
        self.fields['vacationActive'].widget.attrs = { 'data-link-display' : 'id_vacationinfo' }

    def clean(self):
        super().clean()
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
        autoreply = self.cleaned_data.get("vacationActive")
        text= self.cleaned_data.get("vacationinfo")
        if(autoreply and not text):
            raise forms.ValidationError(
                _("Para activar la respuesta automática tienes que insertar un texto que se incluirá en el correo.")
            )

    def save(self):
        msg =None
        mail_account = self.get_email
        domain = mail_account.split('@')[1]
        dn='mail=%s,vd=%s,%s' % (mail_account,domain,settings.LDAP_TREE_HOSTING)
        vacation = ldaputils.ldap_bool(self.cleaned_data["vacationActive"])
        text = self.cleaned_data["vacationinfo"]
        try:
            ldap = Ldap()
            data = {
                    'vacationActive' : vacation,
                    'vacationinfo'  : text
                    }
            ldap.modify(dn, data)
            # Nedd to update password in Django, tp notifiy DD
            # TODO: How do we stroe new new pass in django postrge?????
            ldap.unbind()
        except Exception as e:
            msg = "Error mofiying ldap entry: {}".format(e)
            logger.error(msg)
            msg = 'error'
        return msg


class EditAccountForm(CustomDivForm):
    quota =  forms.IntegerField()
    forward_help_text  = _("Puedes reenviar los correos electrónicos entrantes a "
                     "una o más cuentas de correo alternativas. Si quieres "
                     "que se reenvíen a múltiples cuentas, separa cada una "
                     "de ellas con una coma (user1@example.com,user2@example.com. ")
    forwardActive  = forms.BooleanField(label=_("Activar reenvío automático"), required=False,)
    maildrop       = forms.CharField(label=_('Cuentas de destino para el reenvío automático'),
                                         help_text=forward_help_text,widget=forms.Textarea, required=False)

    sendme = forms.BooleanField(label=_("Recibir una copia a mi buzón"), required=False)

    vacationActive = forms.BooleanField(label=_("Activar respuesta automática"), required=False,)
    vacationinfo   = forms.CharField(label=_('Mensaje de respuesta automática'), required=False,
                                        widget=CKEditorWidget(),
                                        help_text=_('Inserta el texto del mensaje de respuesta automática.'))
    
    def __init__(self, *args, **kwargs):
        self.get_email = kwargs.pop('mail_account')
        super(EditAccountForm,self).__init__(*args, **kwargs)

class WhiteListMassSubscription(forms.Form):
    """Form fields to masssubscribe users to a list.
    """
    emails = widgets.ListOfStringsField(
        label=_('Añade múltiples destinatarios permitidos'),
        help_text=mark_safe(_(
            'Inserta un correo en cada línea. '
            'Formatos permitidos: <br />'
            'John Doe jdoe@example.com<br />'
            '\"John Doe\" jdoe@example.com<br />'
            'jdoe@example.com (John Doe)<br />'
            )),
    )

class WhiteListMassRemoval(forms.Form):

    """Form fields to remove multiple list users.
    """
    emails = widgets.ListOfStringsField(
        label=_('Elimina correos permitidos'),
        help_text=_('Inserta un correo en cada línea'),
    )

    class Meta:

        """
        Class to define the name of the fieldsets and what should be
        included in each.
        """
        layout = [["Mass Removal", "emails"]]

