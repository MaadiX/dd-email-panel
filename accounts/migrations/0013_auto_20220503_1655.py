# Generated by Django 3.2.12 on 2022-05-03 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0012_auto_20220418_1409'),
    ]

    operations = [
        migrations.AddField(
            model_name='school',
            name='school_domain',
            field=models.CharField(max_length=255, null=True, verbose_name='School Apps Domain'),
        ),
        migrations.AlterField(
            model_name='school',
            name='api_url',
            field=models.URLField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='school',
            name='domain',
            field=models.CharField(max_length=255, null=True, verbose_name='Mail Domain'),
        ),
    ]
