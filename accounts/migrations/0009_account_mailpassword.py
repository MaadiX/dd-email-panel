# Generated by Django 3.2.12 on 2022-04-17 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_alter_account_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='mailpassword',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
