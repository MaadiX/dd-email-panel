from django.db import models

# Create your models here.
# accounts/models.py
from django.utils.translation import ugettext_lazy as _, get_language
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.conf import settings
from api.ldap import utils

""" 
We need a unique name for  school to identify it
in the main ldap tree.
Each school is managing its own whitelist which is 
stored in the smtp server which stores all schools' whitelist.
Usiing a school name we distinguish one from the others
and we can return a list of own whitelisted emails instead of the whole list.
All data for the school, such as domain, apidomain, quota , etc
are difined in local_settings.py
"""

class AccountManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, keycloak_id, email, first_name, last_name, tags, quota, mailpassword,password, **extra_fields):
        values = [username, email, first_name, last_name, quota]
        field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
        for field_name, value in field_value_map.items():
            if not value:
                raise ValueError('The {} value must be set'.format(field_name))

        email = self.normalize_email(email)
        user = self.model(
            username = username,
            email = email,
            first_name = first_name,
            last_name = last_name,
            quota = quota,
            mailpassword = mailpassword,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, keycloak_id, email, first_name=None, last_name=None,tags=None,  quota=None, mailpassword=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, first_name, last_name, quota, password, **extra_fields)

    def create_superuser(self, username, email, first_name=None, last_name=None, tags=None,  quota=None, mailpassword=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, keycloak_id=None, email=None, tags=None, first_name=None, last_name=None, quota=None, mailpassword=None, password=None, **extra_fields) 


class Account(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=150, unique=True)
    keycloak_id = models.CharField(max_length=150, null=True, blank=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=150, null=True, blank=True)
    last_name = models.CharField(max_length=150, null=True, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(null=True)
    tags = ArrayField(models.CharField(max_length=200),verbose_name=_("Groups"), null=True, blank=True )
    quota = models.CharField(max_length=64, null=True, blank=True)
    mailpassword = models.CharField(max_length=200, null=True, blank=True)

    objects = AccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    class Meta:
         
        permissions = (
            ("can_edit_own_account", "Edit own account"),
            ("can_edit_student_account", "Edit student account"),
            ("can_edit_own_student_account", "Edit own stuednt account"),
            ("can_edit_whitelist", "Edit whitelist"),
            )
    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username.split()[0]

class MailDomain(models.Model):

    domain = models.CharField(
        max_length = 255,
        null = True,
        unique = True,
        verbose_name = _('Email Domain')
    )
    smtp_server = models.CharField(
        max_length = 255,
        null = True,
        blank = True,
        verbose_name = _('SMPT Server')
    )
    security =models.CharField(
        max_length = 16,
        null = True,
        verbose_name = _('Connection Security'),
        default = 'SSL/TLS',
    )

    smtp_port = models.IntegerField(
        null = True,
        verbose_name = _('SMPT Port'),
        default = 465,
    ) 
    mx_server = models.CharField(
        max_length = 512,
        null = True,
        verbose_name = _('MX Server')
    )
    imap_port = models.IntegerField(
        null = True,
        verbose_name = _('IMAP Port'),
        default = 993,
    )
    pop_port = models.IntegerField(
        null = True,
        verbose_name = _('IMAP Port'),
        default = 995,
    )
    auth_method = models.CharField(
        max_length = 100,
        null = True,
        verbose_name = _('SMPT Security'),
        default = _('Normal Password'),
    )

    def __str__(self):
        return self.domain


class Role(Group):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = _('Role')
