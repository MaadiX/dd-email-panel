import json, requests, sys, logging
import os

from django.test import TestCase
from django.test.runner import DiscoverRunner
from django.conf import settings
sys.path.append('../api')
from api import utils
from api.ldap import utils as ldaputils

class NoDbTestRunner(DiscoverRunner):
    """ A test runner to test without database creation """

    def setup_databases(self, **kwargs):
        """ Override the database creation defined in parent class """
        pass

    def teardown_databases(self, old_config, **kwargs):
        """ Override the database teardown defined in parent class """
        pass

    def test_create_json(self):
        expected_data={
            "config" : {
                "inbound_host"  : settings.IMAP_SERVER,
                "inbound_port"  :  settings.IMAP_PORT,
                "inbound_ssl_mode" : 'ssl',
                "outbound_host" : settings.SMTP_SERVER,
                "outbound_port" :    'ssl',
                "outbound_ssl_mode": settings.MAIL_SECURITY,
            },
            "mails" : [{
                "email"         : "u@test.com",
                "user_id"       : "u",
                "name"          : "u"  + ' ' + "last",
                "password"      : "test",
                },
                {
                "email"         : "u@test.com",
                "user_id"       : "u",
                "name"          : "u"  + ' ' + "last",
                "password"      : "test",
                }
                ]
        }  

        dd_data = [{
            "email"         : "u@test.com",
            "user_id"       : "u",
            "name"          : "u"  + ' ' + "last",
            "password"      : "test",
            },
            {
            "email"         : "u@test.com",
            "user_id"       : "u",
            "name"          : "u"  + ' ' + "last",
            "password"      : "test",
            }
            ] 
        json_data = utils.build_users_mail_josn() 
        print("NC DATA ", json_data)
        self.assertEqual(json_data, json.dumps(expected_data))

