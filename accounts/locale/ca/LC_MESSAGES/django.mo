��            )   �      �     �     �     �  *   �        /        D     _     l  	   s  6   }      �     �  	   �     �      
     +     J     Z     c  6   {  �   �     �     �  	   �     �     �     �  B  �     /     7     V  (   s  	   �  /   �     �     �       	     8     #   J  !   n     �     �  #   �     �     �     	  "   	  6   0	  �   g	  "   C
     f
  	   j
     t
     �
     �
                                                                          	   
                                                             Activado Activar reenvío automático Activar respuesta automática Añade múltiples destinatarios permitidos Connection Security Cuentas de destino para el reenvío automático Elimina correos permitidos Email Domain Groups IMAP Port Inserta el texto del mensaje de respuesta automática. Inserta un correo en cada línea Las contraseñas no coinciden. MX Server Mail Domain Mensaje de respuesta automática No has cambiado ningún valor. Normal Password Password Please Confirm Password Por favor confirma la contraseña que has introducido. Puedes reenviar los correos electrónicos entrantes a una o más cuentas de correo alternativas. Si quieres que se reenvíen a múltiples cuentas, separa cada una de ellas con una coma (user1@example.com,user2@example.com.  Recibir una copia a mi buzón Role SMPT Port SMPT Security SMPT Server School Apps Domain Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Activat Activar reenviament automàtic Activar resposta automàtica Afegeix múltiples destinataris permesos Seguretat Comptes de destí per el reenviament automàtic Elimina correus permesos Domini del correu Grups Port imap Introdueix el text del missatge de resposta automàtica. Introdueix un correu en cada línia Les contrasenyes no coincideixen. Servidor imap Domini del correu Missatge de la resposta automàtica No has canviat cap valor. Contraseña normal Contrasenya Si us plau confirma la contrasenya Si us plau confirma la contrasenya que has introduït. Pots reenviar els correus electrònics entrants a una o més comptes deqa correu alternatives. Si vols que es reenviïn a múltiples comptes, separa cadascuna d'elles amb una coma (user1@example.com,user2@example.com).  Rebre una còpia a la meva bústia Rol Port SMTP Seguretat SMTP Servidor SMTP Domini de la escola 