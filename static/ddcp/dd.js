/*
* Copyright 2017 the Isard-vdi project authors:
*      Josep Maria Viñolas Auquer
*      Alberto Larraz Dalmases
* License: AGPLv3
*/

/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */

/// Load common header from api ///
//
url =  window.location.hostname.split('.').slice(1).join('.');
api_url = "https://api." + url  +"/header/html/admin"
ajax( api_url, function( data ){
  document.getElementById("header").innerHTML = data;
});

function getXmlHttpObject() {
    var xmlHttp;
    try {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    } catch (e) {
        // Internet Explorer
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    if (!xmlHttp) {
        alert("Your browser does not support AJAX!");
    }
    return xmlHttp;
}


function ajax(url, onSuccess, onError) {

    var xmlHttp = getXmlHttpObject();

    xmlHttp.onreadystatechange = function() {
      if (this.readyState === 4) {

            // onSuccess
            if (this.status === 200 && typeof onSuccess == 'function') {
                onSuccess(this.responseText);
            }

            // onError
            else if(typeof onError == 'function') {
                onError();
            }

        }
    };
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
    return xmlHttp;
}





function toggleCollapsible(trigger)
{
    var target_id     = trigger.getAttribute('data-toggle');
    var target        = document.querySelector('#'+target_id);
    var excluded      = trigger.getAttribute('data-excluded');
    var is_collapsed  = target.classList.contains('collapsed');
    if(excluded){
        [...document.querySelectorAll('.' + excluded)].forEach(
            function(trigger){
                trigger.classList.add('collapsed')
            }
        );
    }
    if(is_collapsed)
        target.classList.remove('collapsed');
    else
        target.classList.add('collapsed');
}

document.addEventListener("DOMContentLoaded", function()
{
    var hamburger = document.getElementsByClassName("hamburguer")[0];
    if (typeof(hamburger) != 'undefined' && hamburger != null) {
      document.querySelector('.hamburguer').addEventListener('click', function(){
          document.body.classList.toggle('navigation-open');
      });
    }

    [...document.querySelectorAll('[data-toggle]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
                toggleCollapsible(trigger);
            });
        }
    );

});


