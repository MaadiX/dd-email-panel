# DD Email Panel


## Description


Django interface to manage email accounts stored in ldap. It has been developed for the [digitaldemocratic project](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/tree/master) and consists of a graphical interface that allows to:


- Edit emails accounts (password, forward and automatic reply).
- Manage accounts allowed to send and receive mail (whitelist). The email system is designed for exclusive use in the educational environment and by default only allows communication with email accounts of the schools that are members of the project. However, it is possible to add emails to a whitelist in order to allow communication with external accounts (parents, institutions, etc).


This interface does not contemplate the creation of new accounts, which is carried out from the [main admin interface](https://gitlab.com/isard/isard-sso/-/tree/api-jwt/admin)
    
The accounts are received from the api of the main admin interface. 

So, this application  may be useless out the the Digitaldemocratic context.


## Requirements

- Python 3.X + virtualenv
- xmlsec1
- Postgresql  To store user accounts with access to the interface. 
  Mysql would not work because PostgreSQL specific fields are used. 
  [See django postgres fields](https://docs.djangoproject.com/en/4.0/ref/contrib/postgres/fields/)
     
- OpenLdap (to store mail accounts)
  This interface needs to connect to a LDAP server for reading/writing mail account data.
  For mail accounts it uses [Phamm schema](https://github.com/F0rth/Phamm/blob/master/schema/ldif/phamm.ldif)


##Installation  

To integrate this application into the digitaldemoctratic environment, you have to install it using the subdomain
correu.SCHOOL.DOMAIN.TLD. 


This is an application created with django. You will need to clone the repository and create a virtualenvironment.

```
git clone https://gitlab.com/MaadiX/dd-email-panel.git

cd dd-email-panel
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt

```

copy the files `dd-email-panel/ddcp/local_settings_example.py` to `dd-email-panel/ddcp/local_settings.py`
and dd-email-panel/ddcp/secret_settings_example.py to dd-email-panel/ddcp/secret_settings.py
Assign your custom values to the variables.

Mainly you need to include values for postgre and ldap connection.
The SITE_URL variable is used by the saml2 extension to connect to the idp.

## Configiration

Some manual steps are required in order to complete the installation

### Authentication

Allows two forms of authentication:

1) To the Django admin (required). For that from the directory where the file manage.py is, run:

`python3 manage.py createsuperuser`

2) To the forntend, via saml authentication. Below yoy will find instructions on how to configure saml authentication


### Import Users

There are two Django commands available to see and import users from the school api:

Getting a list of available users: 

``python3 manage.py getapiusers``

Importing users from api into django:  

``python3 manage.py importusers``  


The importusers command will create users in postgre so that they can login via saml as well as their corresponding email accounts in ldap, so that they can start sending and receiving emails.

Obviously, if the domain of an email does not match the one of the school, the corresponding entry in ldap will not be created.  

### Configure saml authentication in keycloak

To allow newly imported accounts to login to the interface, it is necessary to register the client in keycloak.
The keycloak is accessed at sso.SCHOOL.DOMAIN.TLD.

Click on Client in the left column and click on 'Create' to register the client in keycloak.

This screenshot shows all the necessary values. In each field you will have to replace the domain test.digitaldemocratic.net by the SCHOOL.DOMAIN.TLD.

![Enable saml](imgs/mail-Keycloak_Admin_Console.png)

Once the client is created, click on the 'Mappers' tab and add email and username, as shown in this image.

![Set Mappers](imgs/mapper_keycloak.png)

### Configure the application to connect to the idp

By default, in order to verify that the authentication works, we have left the retrieval of metadata via remote url enabled.
However, according to the documentation it is reommended to store the corresponding file locally.
https://djangosaml2.readthedocs.io/contents/setup.html#metadata

For that, access the idp metadata file at https://sso.DOMINIO.DE.LA.ESCUELA.TLD/auth/realms/master/protocol/saml/descriptor
and save it on the server in `/installation/path/ddcp/ddcp/ddcp/idp_metadata.xml`
    (in the same folder as urls.py).

We have not automated this step, so that a manual check of the authenticity of the xml file can be done.
**TODO** Check if this file can be exported from Keycload adnmin interface.  

## Usage

[User guide](https://dd.digitalitzacio-democratica.xnet-x.net/docs-category/correu/)

## Project status
This project is still under construction.

TODO:
- [ ]Socket.io Connection to share data with DigitalDemocratic API
