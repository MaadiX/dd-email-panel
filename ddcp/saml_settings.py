from os import path
import saml2
import saml2.saml
from ddcp.local_settings import *

BASEDIR = path.dirname(path.abspath(__file__))
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/login/'
LOGOUT_REDIRECT_URL = '/login/'
ACS_DEFAULT_REDIRECT_URL = '/'
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SAML_IGNORE_LOGOUT_ERRORS = True

# TODO: we could use this default settings , so a user that logs in will have
#their mail created???
SAML_CREATE_UNKNOWN_USER = False
SAML_ATTRIBUTE_MAPPING = {
    'username': ('username', ),
    'email': ('email', ),
    'tags': ('groups', ),
}

# Used by SAML_CONFIG to check where to get idp metadata
# Recommended config is not to use remote
# See https://djangosaml2.readthedocs.io/contents/setup.html#metadata
# If the ipd.xml has been manually imported into the server from
# https://sso.SCHOOL_DOMAIN/auth/realms/master/protocol/saml/descriptor
# and svaed in BASEDIR, 'idp_metadata.xml' , this will be used.
# otherwise  a remote connection will be used.
# You should also save the ipd RSA certificate in 
# path.join(BASEDIR, 'certs/idp-dd.cert'

file_path = path.join(BASEDIR, 'idp_metadata.xml')
# Create the dictionaty for 'metadata'

METADATA = {}
if (path.exists(file_path)):
    METADATA["local"] =  [path.join(BASEDIR, file_path)]
          
else:
    remote_data = {}
    remote_data["url"]='https://sso.' + SCHOOL_DOMAIN + '/auth/realms/master/protocol/saml/descriptor'
    if(path.exists(path.join(BASEDIR, 'certs/idp-dd.cert'))):
        remote_data["cert"] =path.join(BASEDIR, 'certs/idp-dd.cert')

    METADATA["remote"]=[]
    METADATA["remote"].append(remote_data)


SAML_CONFIG = {
  # full path to the xmlsec1 binary programm
  'xmlsec_binary': '/usr/bin/xmlsec1',

  # your entity id, usually your subdomain plus the url to the metadata view
  'entityid': SITE_URL +'metadata/',

  # directory with attribute mapping
  #'attribute_map_dir': path.join(BASEDIR, 'attribute-maps'),

  # Permits to have attributes not configured in attribute-mappings
  # otherwise...without OID will be rejected
  'allow_unknown_attributes': True,

  # this block states what services we provide
  'service': {
      # we are just a lonely SP
      'sp' : {
          'name': 'Federated Django sample SP',
          'name_id_format': saml2.saml.NAMEID_FORMAT_TRANSIENT,

          # For Okta add signed logout requests. Enable this:
          # "logout_requests_signed": True,

          'endpoints': {
              # url and binding to the assetion consumer service view
              # do not change the binding or service name
              'assertion_consumer_service': [
                  (SITE_URL +'acs/',
                   saml2.BINDING_HTTP_POST),
                  ],
              # url and binding to the single logout service view
              # do not change the binding or service name
              'single_logout_service': [
                  # Disable next two lines for HTTP_REDIRECT for IDP's that only support HTTP_POST. Ex. Okta:
                  #(SITE_URL + 'saml2/ls/',
                  # saml2.BINDING_HTTP_REDIRECT),
                  (SITE_URL +'/ls/post',
                   saml2.BINDING_HTTP_POST),
                  ],
              },

          'signing_algorithm':  saml2.xmldsig.SIG_RSA_SHA256,
          'digest_algorithm':  saml2.xmldsig.DIGEST_SHA256,

           # Mandates that the identity provider MUST authenticate the
           # presenter directly rather than rely on a previous security context.
          'force_authn': False,

           # Enable AllowCreate in NameIDPolicy.
          'name_id_format_allow_create': False,

           # attributes that this project need to identify a user
          'required_attributes': ['username'],

           # attributes that may be useful to have but not required
          'optional_attributes': ['email'],

          #'want_response_signed': True,
          #'authn_requests_signed': True,
          #'logout_requests_signed': True,
          # TODO Set up signin
          'want_response_signed': True,
          'authn_requests_signed': False,
          'logout_requests_signed': False,

          # Indicates that Authentication Responses to this SP must
          # be signed. If set to True, the SP will not consume
          # any SAML Responses that are not signed.
          #'want_assertions_signed': True,
          'want_assertions_signed': True,

          'only_use_keys_in_metadata': True,

          # When set to true, the SP will consume unsolicited SAML
          # Responses, i.e. SAML Responses for which it has not sent
          # a respective SAML Authentication Request.
          'allow_unsolicited': True,


          },
      },

  # where the remote metadata is stored, local, remote or mdq server.
  # One metadatastore or many ...
  'metadata' :  METADATA ,

  # set to 1 to output debugging information
  'debug': 1,

  # Encryption
  #'encryption_keypairs': [{
  #    'key_file': path.join(BASEDIR, 'private.key'),  # private part
  #    'cert_file': path.join(BASEDIR, 'public.pem'),  # public part
  #}],logout

  # own metadata settings
  }

# Load signing keys if exists
# Signing
# TODO: to activate signin by client. Keykloack is not recognizing public key
# Generated certficiates with
# openssl req -x509 -sha256 -nodes -newkey rsa:2048 -keyout priv_Key.key -out pub_key.pem
