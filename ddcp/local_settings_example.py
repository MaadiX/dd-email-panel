#import ldap
#from django_auth_ldap.config import LDAPSearch, LDAPSearchUnion, LDAPGroupQuery
from django.conf import settings

SITE_URL = 'PLACEHOLDER_SITE_URL'
SCHOOL_DOMAIN = 'PLACEHOLDER_SCHOOL_DOMAIN'
ALLOWED_HOSTS = [SITE_URL]
SCHOOL_MAIL_DOMAIN = 'PLACEHOLDER_SCHOOL_MAIL_DOMAIN'
SCHOOL_API_URL = 'https://admin.' + SCHOOL_DOMAIN
DEFAULT_QUOTA = '1073741824'


SMTP_SERVER = "PLACEHOLDER_SMTP_SERVER"
IMAP_SERVER = "PLACEHOLDER_IMAP_SERVER"
POP3_SERVER = IMAP_SERVER
IMAP_PORT   = 993 
POP3_PORT   = 995 
SMTP_PORT   = 465 
AUTH_PASS   = "normal"
MAIL_SECURITY= "SSL/TLS"


# django ldap auth
LDAPTLS_CACERT= '/etc/ssl/certs/ca-certificates.crt'
AUTH_LDAP_START_TLS   = True
LDAP_AUTH_URL         = "PLACEHOLDER_AUTH_URL"
AUTH_LDAP_SERVER_URI  = LDAP_AUTH_URL
#AUTH_LDAP_BIND_AS_AUTHENTICATING_USER = True
AUTH_LDAP_BIND_DN = "PLACEHOLDER_UTH_LDAP_BIND_DN "

# This is to allow users authetication by email (for changing their own pass and settings) as well as admin user
# Meybe we remove admin user
#AUTH_LDAP_USER_SEARCH = LDAPSearchUnion(
#            LDAPSearch("dc=example,dc=tld", ldap.SCOPE_ONELEVEL, "(cn=%(user)s)"),
#            LDAPSearch("o=hosting,dc=example,dc=tld",ldap.SCOPE_SUBTREE,"(mail=%(user)s)"),
#            )

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'PLACEHOLDER_PSQL_DATABASE',
        'USER': 'PLACEHOLDER_PSQL_USER',
    },
}


DATABASE_ROUTERS = ['ldapdb.router.Router']
