# The URL of the LDAP server.
#LDAP_AUTH_URL = "ldap://localhost:389"
# Initiate TLS on connection.
# TODO: Set to True once we enable remote connetion
LDAP_AUTH_USE_TLS = False
# Set connection/receive timeouts (in seconds) on the underlying `ldap3` library.
LDAP_AUTH_CONNECT_TIMEOUT = None
LDAP_AUTH_RECEIVE_TIMEOUT = None
# 
# LDAP tree
LDAP_TREE_BASE       = "dc=example,dc=tld"
LDAP_TREE_HOSTING    = "o=hosting,%s"             % LDAP_TREE_BASE
LDAP_TREE_USERS      = "ou=sshd,ou=People,%s"     % LDAP_TREE_BASE
LDAP_FILTERS_EMAILS  = "(objectClass=VirtualMailAccount)"
LDAP_FILTERS_METAINFO = "(objectClass=metaInfo)"

# We use a different tree for whitelist
LDAP_WHITELIST_BASE  = "o=mails,dc=whitelist,dc=tld"
LDAP_FILTERS_WITELIST= "(objectClass=inetOrgPerson)"

LDAP_ALLOWED_DOMAINS_BASE  = "o=domains,dc=whitelist,dc=tld"
LDAP_FILTERS_ALLOWED_DOMAINS = "(objectClass=inetOrgPerson)"
