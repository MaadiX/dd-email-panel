"""ddcp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from accounts.views import views_accounts as accounts
from accounts.views import views_apiaccount as apiaccount
from django.conf.urls import i18n

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('api/users/', apiaccount.manage_users, name="api-maanageusers"),
    path('admin/', admin.site.urls),
    path('', include('djangosaml2.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # Self account views
    path('', accounts.LoginRedirect, name='home'),
    path('profile/view/', accounts.ProfileView.as_view(), name='profile'),
    path('profile/forward/', accounts.EditForwardView.as_view(), name='forward'),
    path('profile/vacation/', accounts.EditVacationView.as_view(), name='vacation'),
    path('profile/password/', accounts.ChangePasswordView.as_view(), name='change-password'),
    # Other users edit views
    path('accounts/edit-prof/<pk>/', accounts.AccountProfileView.as_view(), name='account-profile'),
    path('accounts/edit-pass/<pk>/', accounts.AccountPasswordView.as_view(), name='account-pass'),
    path('accounts/edit-forw/<pk>/', accounts.AccountForwardView.as_view(), name='account-forw'),
    path('accounts/edit-vac/<pk>/', accounts.AccountVacationView.as_view(), name='account-vac'),
    path('email-client/', accounts.EmailClientView.as_view(), name='email-client'),
    path('list-users/', accounts.ListUsersView.as_view(), name='list-users'),
    # Whitelist View list
    path('allowed/list/', accounts.WhiteListUsersView.as_view(), name='whitelist-list'),
    path('allowed/add/', accounts.WhiteListMassAddView.as_view(), name='whitelist-add'),
    path('allowed/remove/', accounts.WhiteListMassRemoveView.as_view(), name='whitelist-remove'),
    path('allowed/domains/', accounts.AllowedDomainsView.as_view(), name='allowdomains-list'),
    path('pubkey/', accounts.pub_key_view, name='public-key'),
]
